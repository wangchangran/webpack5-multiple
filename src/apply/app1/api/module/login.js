import { getRequest } from '@/axios/index.js'

export const wxWorkLogin = (params) => {
  return getRequest('/authenticationSchoolBus/login', params)
}
