const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
//---------------------打包分析------------------------------
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin;

//-------------build清除dist目录---------------------------------------
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
//-------------css分块---------------------------------------
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// -------------css压缩----------------------------------------------
const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
// --------------打包时间--------------------------------------------
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');
const smp = new SpeedMeasurePlugin();
// --------------------gzip压缩--------------------
const CompressionPlugin = require('compression-webpack-plugin');
const { merge } = require('webpack-merge');
const CommonConfig = require('./webpack.common.js')
const ProdConfig = (env) => {
  console.log('---------'+process.env.FILE_NAME+'---------'); //app1
  return {
    output: {
      path: path.resolve(__dirname, '../dist', `./${process.env.FILE_NAME}`),
      filename: 'static/js/[name].[contenthash].js',
      publicPath: './',
      //懒加载模块 或 分块模块 的 输出路径 以及命名 不写则默认filename路径 name为vendors
      chunkFilename: 'static/js/[name].[contenthash].js'
    },
    mode: 'production',
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                esModule: false,
              },
            },
            {
              loader: 'css-loader',
              options: { importLoaders: 1 }
            },
            'postcss-loader'
          ]
        },
        {
          test: /\.s[ac]ss$/i,
          use: [{
            loader: MiniCssExtractPlugin.loader,
            options: {
              esModule: false,
            },
          }, "css-loader", 'postcss-loader', "sass-loader",
          {
            loader: 'style-resources-loader',
            options: {
              patterns: [
                path.resolve(
                  __dirname,
                  `../src/apply/${process.env.FILE_NAME}/static/css/glob.scss`
                )
              ]
            }
          }]
        },
      ]
    },
    plugins: [
      //开启gzip压缩
      // new CompressionPlugin({
      // 	algorithm: "gzip",
      // 	test: /\.js$|\.css$/,
      // }),
      //打包分析
      // new BundleAnalyzerPlugin(),
      // 打包清除插件
      new CleanWebpackPlugin(),
      //css分块
      new MiniCssExtractPlugin({
        filename: 'static/css/[name].[contenthash].css',
        chunkFilename: 'static/css/[id].[contenthash].css',
      }),
      //以路经为id
      new webpack.ids.HashedModuleIdsPlugin({
        context: __dirname,
        hashDigestLength: 10
      }),

    ],
    optimization: {
      moduleIds: 'deterministic',
      emitOnErrors: true,
      splitChunks: {
        chunks: 'all',
        automaticNameDelimiter: '~',
        minSize: 30000,
        cacheGroups: {
          vue: {
            test: /[\\/]node_modules[\\/]vue[\\/]/,
            name: 'vue',
            chunks: 'all'
          }
        }
      },
      minimize: true,
      // 压缩
      minimizer: [
        new TerserPlugin(
          {
            cache: true,
            parallel: true,
            // 不生成txt文档
            extractComments: false
            // terserOptions: {
            // 	output: {
            // 		comments: false,
            // 	},
            // },
          }
        ),
        new OptimizeCSSAssetsPlugin({parallel: 4})
      ],
      // 将包含chunks映射关系单独提出出来,避免main.js hash变动
      // runtimeChunk: 'single',
      runtimeChunk: {
        name: (entrypoint) => `runtime~${entrypoint.name}`
      }
    },

  };
};
module.exports = merge(CommonConfig, ProdConfig());

// 打包时间分析 升级5后 未解决
// module.exports = smp.wrap(config);
