const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const IsProd = process.env.NODE_ENV === 'production'
console.log('__dirname',__dirname);
module.exports = {
 
  //是否显示源码
  devtool: IsProd ? false : 'inline-source-map',

  //显示信息
  stats: IsProd ? 'normal' : 'errors-warnings', // errors-warnings errors-only
  entry: path.resolve(__dirname, `../src/apply/${process.env.FILE_NAME}/main.js`),
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: path.resolve(__dirname, '../src'),
        // exclude: (file) =>
        //   /node_modules/.test(file) && !/\.vue\.js/.test(file)
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 8 * 1024,
          name: 'static/img/[name].[contenthash].[ext]',
          // 解决版本问题
          esModule: false
        }
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'static/media/[name].[contenthash].[ext]'
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: 'static/fonts/[name].[contenthash].[ext]'
        }
      }
    ]
  },
  plugins: [

    new VueLoaderPlugin(),
    new webpack.DefinePlugin({
      'process.env.FILE_NAME': JSON.stringify(process.env.FILE_NAME),
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      inject:'body',
      template: path.resolve(
        __dirname,
        `../src/apply/${process.env.FILE_NAME}/${process.env.FILE_NAME}.html`
      )
    }),

  ],
  resolve: {
    alias: {
      //配置别名
      '@': path.resolve(__dirname, '../src'),
      '@app': path.resolve(__dirname, `../src/apply/${process.env.FILE_NAME}`),
    },
    //配置拓展文件 /src/router  === /src/router/index.js
    extensions: ['.js', '.vue', '.json']
  }
};