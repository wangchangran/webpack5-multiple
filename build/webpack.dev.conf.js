const webpack = require('webpack');
const { merge } = require('webpack-merge');
const CommonConfig = require('./webpack.common.js');
const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');

let APICONFIG = require(`../src/apply/${process.env.FILE_NAME}/api/config.js`);

const DevConfig = {
  mode: 'development',
  cache:true,
  devServer: {
    host: '0.0.0.0',
    hot: true,
    // compress:true,
    //不显示页面顶部的 app ready 标栏
    inline: true,
    //   open: true,
    //编译错误时覆盖全屏 显示错误
    overlay: true,
    //   port: 81,
    proxy: {
      // '/api': {
      //   target: 'http://127.0.0.1:3001',
      //   pathRewrite: { '^/api': '' },
      //   changeOrigin: true
      // }
      '/api': {
        target: APICONFIG.CUREVNURL,
        // pathRewrite: { '^/api': '/' },
        ws: false,
        changeOrigin: true
      }
      // '/api': 'http://192.168.0.127:30021'
    }
  },
  entry: path.resolve(__dirname, `../src/apply/${process.env.FILE_NAME}/main.js`),
  output: {
    filename: '[name].js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          {
            loader: "postcss-loader",
          },
        ]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          "vue-style-loader",
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          { loader: "postcss-loader" },
          "sass-loader",
          {
            loader: 'style-resources-loader',
            options: {
              patterns: [
                path.resolve(
                  __dirname,
                  `../src/apply/${process.env.FILE_NAME}/static/css/glob.scss`
                )
              ]
            }
          }
        ]
      },
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new ESLintPlugin()
  ],
};

module.exports = merge(CommonConfig, DevConfig)
