module.exports = (api) => {
  // api.cache(true)
  const presets = [
    [
      '@babel/preset-env',
      {
        useBuiltIns: 'usage',
        corejs: {
          version: 3,
          proposals: true
        },
        targets: {
          browsers: ['> 1%', 'last 2 versions', 'not ie<= 8']
        }
      }
    ]
  ]
  const plugins = [['@babel/plugin-transform-runtime', { corejs: 3 }]]
  if (api.env('production')) {
    plugins.push('transform-remove-console')
  }
  return {
    presets,
    plugins,
    env: {
      test: {
        plugins: ['transform-require-context'],
        presets: [['@babel/preset-env', { targets: { node: 'current' } }]]
      }
    }
  }
}
